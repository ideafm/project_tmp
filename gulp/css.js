import config from '../config';

import gulp from 'gulp';

gulp.task('css', () => {
	return gulp.src(config.PATH.css.src)
		.pipe(gulp.dest(config.PATH.css.dest));
});