import config from '../config';

import gulp from 'gulp';

gulp.task('js:vendor', () => {
	return gulp.src(config.PATH.js.vendorSrc)
		.pipe(gulp.dest(config.PATH.js.vendorDest));
});