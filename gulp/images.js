import config from '../config';

import gulp from 'gulp';
import imagemin from 'gulp-imagemin';

gulp.task('images', () => {
	return gulp.src(config.PATH.images.src)
		.pipe(imagemin())
		.pipe(gulp.dest(config.PATH.images.dest))
});

gulp.task('watch:images', () => {
	let w = gulp.watch(config.PATH.images.src, gulp.parallel('images'));
	w.on('unlink', (filepath) => {
		let filePathFromSrc = path.relative(path.resolve(config.PATH.src), filepath);
		let destFilePath = path.resolve(config.PATH.dest, filePathFromSrc);
		del.sync(destFilePath);
	})
});