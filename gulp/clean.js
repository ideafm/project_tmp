import config from '../config'

import gulp from 'gulp';
import del from 'del';

gulp.task('clean', (cb) => {
	if(!config.isDevelopment) {
		return del(config.PATH.clean);
	} else {
		cb();
	}
});

gulp.task('clean:hard', (cb) => {
	return del(config.PATH.clean);
});