import config from '../config';

import gulp from 'gulp';
import fs from 'fs';
let g = require('gulp-load-plugins')();
let through2 = require('through2').obj;

let jadeSingleFile = (src) => {
	gulp.src(src)
		.pipe(g.plumber())
		.pipe(g.debug({title: 'jade-single'}))
		.pipe(g.jade({
			pretty: true
		}))
		.pipe(gulp.dest(config.PATH.jade.dest))
};

gulp.task('jade:main', () => {
	let manifest = {};
	try {
		manifest = JSON.parse(fs.readFileSync(config.PATH.jade.manifestPath));
	} catch (e) { }

	return gulp.src(config.PATH.jade.src)
		.pipe(g.newer({
			dest: config.PATH.jade.dest,
			ext: '.html'
		}))
		.pipe(g.debug({title: 'jade-main'}))
		.pipe(through2((file, enc, callback) => {
			let content = fs.readFileSync(file.path, enc);
			let res = content.match(/^[\t\n ]*include\s["']?\S+["']?[\n\t ]?$/mg);
			// console.log('file = ', file.basename, ' res = ', res);
			if(res && res.length) {
				let arr = [];
				for(let i = 0, len = res.length; i < len; i++) {
					let inc = res[i].match(/[^\s"'\n\f\r\v\t]+/gm)[1];
					arr.push(inc);
				}
				manifest[file.basename] = arr;
			}
			// console.log('res', res, 'file = ', file.basename);
			fs.writeFileSync(config.PATH.jade.manifestPath, JSON.stringify(manifest));
			g.util.log(g.util.colors.bgGreen('manifest was updated'));
			callback(null, file);
		}))
		.pipe(g.plumber())
		.pipe(g.jade({
			pretty: true
		}))
		.pipe(gulp.dest(config.PATH.jade.dest))
});

gulp.task('jade:manifest', () => {

	let man = {};

	return gulp.src(config.PATH.jade.src)
		.pipe(through2((file, enc, callback) => {
			let content = fs.readFileSync(file.path, enc);
			let res = content.match(/^[\t\n ]*include\s["']?\S+["']?[\n\t ]?$/mg);
			// console.log('file = ', file.basename, ' res = ', res);
			if(res && res.length) {
				let arr = [];
				for(let i = 0, len = res.length; i < len; i++) {
					let inc = res[i].match(/[^\s"'\n\f\r\v\t]+/gm)[1];
					arr.push(inc);
				}
				man[file.basename] = arr;
			}
			// console.log('res', res, 'file = ', file.basename);
			callback()
		}, (callback) => {
			// console.log('man = ', man);
			fs.writeFileSync(config.PATH.jade.manifestPath, JSON.stringify(man));
			callback()
		}));
});

gulp.task('watch:jade-includes', () => {
	let manifest = {};
	try {
		manifest = JSON.parse(fs.readFileSync(config.PATH.jade.manifestPath));
	} catch (e) { }
	let w = gulp.watch(config.PATH.watch.jade.includes);
	w.on('change', (f) => {
		let src = [];
		Object.keys(manifest).forEach((item, i, arr) => {
			for(let i = 0, len = manifest[item].length; i < len; i++) {
				if(f.indexOf(manifest[item][i]) != -1) {
					src.push(`${config.PATH.jade.baseDir}/${item}`);
					// src.push('frontend/jade/'+item);
				}
			}
		});
		if(src.length) {
			jadeSingleFile(src);
		}
		g.util.log(g.util.colors.green('Changed: '), f);
	});
});


gulp.task('watch:jade-main', () => {
	gulp.watch(config.PATH.jade.src, gulp.series('jade:main'))
});

gulp.task('watch:jade', gulp.parallel('watch:jade-main', 'watch:jade-includes'));
gulp.task('jade', gulp.parallel('jade:manifest', 'jade:main'));