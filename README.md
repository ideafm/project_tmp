## install
```
npm i
```

##gulp
- Добавить `./node_modules/.bin` в переменную PATH
- Если используется fish-shell, то можно до добавить в $fish_user_paths <br> `set -U fish_user_paths ./node_modules/.bin $fish_user_paths`
- Либо запускать следующим образом: `./node_modules/.bin/gulp` 

##jade
Собираются корневые файлы `file1, file2, file3`
```
jade/
    subdir1/
        ...// *.jade
    subdir2/
        ...// *.jade
    file1.jade
    file2.jade
    file3.jade
```
Из include'ов корневых файлов автоматически собирается `jadeManifest.json`

##js
Собираются корневые файлы `js1, js2, js3` по аналогии c jade. При `NODE_ENV=production` скрипты будут сжатыми без sourceMaps

##stylus
Собирает корневой файл `stylus.styl`. При `NODE_ENV=production` стили будут сжатыми без sourceMaps

##usage
1. `gulp`

    В этом случае порядок следующий: параллельно выполняются таски webpack (смотрит за изменениями js), jade, stylus, images, fonts, далее запускается watch, который смотрит за измениями файлов и пересобирает их и serve, который запускает сервер и автоматически перезагружает браузер при изменениях.    

2. `NODE_ENV=production gulp`
    
    В этом случае происходит очистка `public` и собираюстся все файлы по возможности с минификацией

