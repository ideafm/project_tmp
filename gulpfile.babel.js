import config from './config'

import gulp from 'gulp';
let g = require('gulp-load-plugins')();

require('require-dir')('./gulp/');

import path from 'path';


const watchTasks = gulp.parallel('watch:fonts','watch:images','watch:jade','watch:stylus');
gulp.task('watch', (cb) => {
	if(!config.isDevelopment) {
		cb();
	} else {
		watchTasks();
	}
});

gulp.task('default', gulp.series(
	'clean',
	gulp.parallel(
		'webpack',
		'js:vendor',
		'jade',
		'css',
		'stylus',
		'images',
		'fonts'
	),
	gulp.parallel(
		'serve',
		'watch'
	)
));
