import path from 'path';
import webpackStream from 'webpack-stream';
let webpack = webpackStream.webpack;

let srcBase = 'frontend';
let destBase = 'public';

const basePath = path.join(__dirname);

const PATH = {
	src: `${srcBase}`,
	dest: `${destBase}`,
	webpack: {
		loaders: {
			include: path.join(__dirname, srcBase)
		},
		output: {
			publicPath: '/js/'
		}
	},
	js: {
		src: `${srcBase}/js/*.js`,
		dest: `${destBase}/js`,
		vendorSrc: `${srcBase}/js/vendor/*.js`,
		vendorDest: `${destBase}/js/vendor`,
		webpack: {
			loaders: {
				include: path.join(__dirname, srcBase)
			}
		}
	},
	jade: {
		src: `${srcBase}/jade/*.jade`,
		dest: `${destBase}`,
		baseDir: `${srcBase}/jade`,
		manifestName: 'jadeManifest.json',
		get manifestPath() {
			return process.cwd() + `/${this.baseDir}/${this.manifestName}`;
		}
	},
	images: {
		src: `${srcBase}/img/**/*`,
		dest: `${destBase}/img`
	},
	fonts: {
		src: `${srcBase}/fonts/**/*`,
		dest: `${destBase}/fonts/`
	},
	stylus: {
		src: `${srcBase}/stylus/styles.styl`,
		dest: `${destBase}/css/`
	},
	clean: `${destBase}/**`,
	watch: {
		jade: {
			includes: [`${srcBase}/jade/**/*.jade`, `!${srcBase}/jade/*.jade`]
		},
		stylus: `${srcBase}/stylus/**/*.styl`
	},
	css: {
		src: `${srcBase}/css/*.css`,
		dest: `${destBase}/css`
	}
};

const autoplefixerOptions = {
	browsers: ['last 2 versions', 'ie >= 9']
};

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'dev';

const webpackConfig = {
	output: {
		publicPath: PATH.webpack.output.publicPath
	},
	watch: isDevelopment,
	devtool: isDevelopment ? 'cheap-module-inline-source-map' : null,
	module: {
		loaders: [{
			test: /\.js$/,
			include: PATH.webpack.loaders.include,
			loader: 'babel?presets[]=es2015'
		}]
	},
	plugins: [
		new webpack.NoErrorsPlugin(),
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			"window.jQuery": "jquery"
		})
	]
};

const browserSync = {
	oprions: {
		server: {
			baseDir: `./${destBase}`
		}
	},
	watch: `${destBase}/**/*.*`
};


export default {
	basePath,
	PATH,
	webpackConfig,
	isDevelopment,
	browserSync,
	autoplefixerOptions
}